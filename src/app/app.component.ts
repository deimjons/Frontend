import { Component } from '@angular/core';
import { AppService } from '../providers/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  constructor(private service: AppService) {

  }

  setShowSidebar() {
    if(window.innerWidth > 992) {
      this.service.setShowSideBar(true);
    } else {
      this.service.setShowSideBar(false);
    }
  }
}
