import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../providers/app.service';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent implements OnInit {
  constructor(private service: AppService) { }

  ngOnInit() {
  }

  hideSearchBar() {
    this.service.setSearchBar(false);
  }

}
