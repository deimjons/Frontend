import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../providers/app.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  searchBar: boolean = false;
  messages: any[] = [];
  notifications: any[] = [];

  constructor(private service: AppService) { }

  ngOnInit() {
    this.service.searchBar.subscribe(flag => {
      this.searchBar = flag;
    });

    this.messages = [
      {group: "group1.png", name: "AirAsia Intranet System", users: ["user1.png", "user2.png", "user3.png"], time: "just now"},
      {group: "group2.png", name: "Web server hardwares task", users: ["user1.png", "user2.png", "user3.png"], time: "18 hrs"},
      {group: "group1.png", name: "Sidebar Toggler", users: ["user1.png", "user2.png", "user3.png"], time: "Yesturday"},
    ]

    this.notifications = [
      {img: "group1.png", title: "You are now a member of AirAsia Intranet System Project", time: "just now", number: 1},
      {img: "group1.png", title: "You have 10 new bugs in Project Internet Managers project", time: "3 mins", number: 10},
      {img: "group1.png", title: "You have 1 new bug in Sidebar Toggler project", time: "10 mins", number: 1},
      {img: "group1.png", title: "None Fix Home 2 has been published", time: "15 hrs", number: 1},
      {img: "group2.png", title: "You have 10 new tasks from john Purpleson", time: "Yesturday", number: 10},
      {img: "group2.png", title: "You have 1 new task from Igor Wang", time: "Yesturday", number: 1},
    ];
  }

  showSearchBar() {
    this.searchBar = true;
  }

  setShowSideBar(flag) {
    this.service.setShowSideBar(flag);
  }

}
