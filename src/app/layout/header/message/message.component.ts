import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'header-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
  @Input() data: any;

  constructor() { }

  ngOnInit() {
  }

}
