import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../providers/app.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  highLightStatus: Array<boolean> = [true];
  pages: Array<string>;
  titles: Array<string>;
  showSidebar: boolean;

  constructor(private service: AppService) { 
    service.showSideBar.subscribe(flag => {
      this.showSidebar = flag;
    });
  }

  ngOnInit() {
    this.pages = ['dashboard', 'newbug', 'newproject', 'cpanel', 'support', 'knowledge'];
    this.titles = ['Dashboard', 'Create New Bug', 'Create New Project', 'Control Panel', 'Contact Support', 'Knowledge Base'];
  }

  setHighlightStatus(index: number) {
    this.highLightStatus = [];
    this.highLightStatus[index] = true;
    this.service.setPage(this.pages[index], this.titles[index]);
    this.service.showProjectTable(false);

    if(window.innerWidth < 992) {
      this.service.setShowSideBar(false);
    }
  }
}
