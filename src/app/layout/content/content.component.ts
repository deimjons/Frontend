import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../providers/app.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  title: string = "Dashboard";
  page: string = "dashboard";
  showProjectTable: boolean = false;

  constructor(private service: AppService) {
    service.page.subscribe(label => {
      this.page = label.page;
      this.title = label.title;
      if(label.page != "dashboard") {
        this.showProjectTable = false;
      }
    });
    service.showTable.subscribe(flag => {this.showProjectTable = flag});
  }

  ngOnInit() {

  }

}
