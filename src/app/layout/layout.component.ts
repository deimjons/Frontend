import { Component, OnInit } from '@angular/core';
import { AppService } from '../../providers/app.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  sidebar: boolean;

  constructor(private service: AppService) { 
    if(window.innerWidth > 992) {
      this.sidebar = true;
    } else {
      this.sidebar = false;
    }

    service.showSideBar.subscribe(flag => {
      this.sidebar = flag;
    });
  }

  ngOnInit() {

  }

}
