import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './layout/header/header.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { ContentComponent } from './layout/content/content.component';
import { RunningProjectComponent } from './components/running-project/running-project.component';
import { ProjectComponent } from './components/running-project/project/project.component';
import { MessagesComponent } from './components/messages/messages.component';
import { MessageWindowComponent } from './components/messages/message-window/message-window.component';
import { MessageUserComponent } from './components/messages/message-user/message-user.component';
import { OneMessageComponent } from './components/messages/message-window/one-message/one-message.component';
import { AppService } from '../providers/app.service';
import { FooterComponent } from './layout/footer/footer.component';
import { SearchbarComponent } from './layout/header/searchbar/searchbar.component';
import { ModalModule, BsDropdownModule } from 'ngx-bootstrap';
import { MessageComponent } from './layout/header/message/message.component';
import { NotificationComponent } from './layout/header/notification/notification.component';
import { ProjectTableComponent } from './components/running-project/project-table/project-table.component';
import { KnowledgeBaseComponent } from './components/knowledge-base/knowledge-base.component';
import { NewBugModalComponent } from './components/new-bug-modal/new-bug-modal.component';
import { TagElementComponent } from './components/tag-element/tag-element.component';
import { FileElementComponent } from './components/new-bug-modal/file-element/file-element.component';
import { FileUploadElementComponent } from './components/file-upload-element/file-upload-element.component';
import { NewProjectModalComponent } from './components/new-project-modal/new-project-modal.component';
import { AccountSettingsModalComponent } from './components/account-settings-modal/account-settings-modal.component';
import { CustomInputElementComponent } from './components/custom-input-element/custom-input-element.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HeaderComponent,
    SidebarComponent,
    ContentComponent,
    RunningProjectComponent,
    ProjectComponent,
    MessagesComponent,
    MessageWindowComponent,
    MessageUserComponent,
    OneMessageComponent,
    FooterComponent,
    SearchbarComponent,
    MessageComponent,
    NotificationComponent,
    ProjectTableComponent,
    KnowledgeBaseComponent,
    NewBugModalComponent,
    TagElementComponent,
    FileElementComponent,
    FileUploadElementComponent,
    NewProjectModalComponent,
    AccountSettingsModalComponent,
    CustomInputElementComponent,
  ],
  imports: [
    BrowserModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
