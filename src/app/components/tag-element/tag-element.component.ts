import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tag-element',
  templateUrl: './tag-element.component.html',
  styleUrls: ['./tag-element.component.scss']
})
export class TagElementComponent implements OnInit {
  @Input() assigned: boolean = true;
  @Input() data: string;
  show: boolean = true;

  constructor() { }

  ngOnInit() {
  }

  hide() {
    this.show = false;
  }

}
