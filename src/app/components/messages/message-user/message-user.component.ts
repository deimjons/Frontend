import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-message-user',
  templateUrl: './message-user.component.html',
  styleUrls: ['./message-user.component.scss']
})
export class MessageUserComponent implements OnInit {
  @Input() data: any;
  users: any[] = [];
  constructor() { }

  ngOnInit() {

  }

}
