import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'one-message',
  templateUrl: './one-message.component.html',
  styleUrls: ['./one-message.component.scss']
})
export class OneMessageComponent implements OnInit {
  @Input() data: any;
  constructor() { }

  ngOnInit() {
  }

}
