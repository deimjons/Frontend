import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-message-window',
  templateUrl: './message-window.component.html',
  styleUrls: ['./message-window.component.scss']
})
export class MessageWindowComponent implements OnInit {
  messages: any[];
  constructor() { }

  ngOnInit() {
    this.messages = [
      {name: "Jason Cheung", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure", image: "user3.png", date: "12:37 AM", direction: "left"},
      {name: "Jason Cheung", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ", image: "user3.png", date: "12:37 AM", direction: "right"},
      {name: "Jason Cheung", text: "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.", image: "user3.png", date: "12:37 AM", direction: "left"},
      {name: "Jason Cheung", text: "Lorem ipsum dolor sit amet, ", image: "user3.png", date: "12:37 AM", direction: "right"},
    ];
  }

}
