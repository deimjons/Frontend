import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {
  users: any[];
  selected: Array<Boolean>;
  constructor() { }

  ngOnInit() {
    this.users = [
      {selected: true, groupImage: "group1.png", groupName: "Airasia intranet system", groupUsers: ["user1.png", "user2.png", "user3.png"], comments: 6, date: "1"},
      {selected: false, groupImage: "group2.png", groupName: "Airasia intranet system", groupUsers: ["user1.png", "user2.png", "user3.png"], comments: 6, date: "1"},
      {selected: false, groupImage: "group2.png", groupName: "Airasia intranet system", groupUsers: ["user1.png", "user2.png", "user3.png"], comments: 6, date: "1"},
      {selected: false, groupImage: "group1.png", groupName: "Airasia intranet system", groupUsers: ["user1.png", "user2.png", "user3.png"], comments: 6, date: "1"},
      {selected: false, groupImage: "group2.png", groupName: "Airasia intranet system", groupUsers: ["user1.png", "user2.png", "user3.png"], comments: 6, date: "1"},
    ];
  }

  selectUser(index) {
    for(let i = 0 ; i < this.users.length ; i ++) {
      this.users[i].selected = false;
    }
    this.users[index].selected = true;
  }

}
