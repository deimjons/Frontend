import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../../../providers/app.service'

@Component({
  selector: 'running-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
  host: {
    "[style.background-color]": "data.color"
  }
})
export class ProjectComponent implements OnInit {
  @Input() data: any;

  constructor(private service: AppService) { }

  ngOnInit() {

  }

  showProjectTable() {
    this.service.showProjectTable(true);
  }

}
