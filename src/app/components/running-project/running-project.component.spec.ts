import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunningProjectComponent } from './running-project.component';

describe('RunningProjectComponent', () => {
  let component: RunningProjectComponent;
  let fixture: ComponentFixture<RunningProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunningProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunningProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
