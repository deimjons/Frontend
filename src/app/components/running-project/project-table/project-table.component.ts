import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../providers/app.service'

@Component({
  selector: 'app-project-table',
  templateUrl: './project-table.component.html',
  styleUrls: ['./project-table.component.scss']
})
export class ProjectTableComponent implements OnInit {
  tableData: any[];
  constructor(private service: AppService) { }

  ngOnInit() {
    this.service.showTable.subscribe(flag => {
      this.tableData = this.service.getProjectTableData();
    });
  }

}
