import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-running-project',
  templateUrl: './running-project.component.html',
  styleUrls: ['./running-project.component.scss']
})
export class RunningProjectComponent implements OnInit {

  projects: any[];

  constructor() { }

  ngOnInit() {
    this.projects = [
      {title: "Website Revamp & Deployment", color: "#f48d32", icon: false},
      {title: "Website Revamp & Deployment", color: "#36c6d3", icon: true},
      {title: "Website Revamp & Deployment", color: "#78e258", icon: false},
      {title: "Website Revamp & Deployment", color: "#7f56ca", icon: false},
      {title: "Website Revamp & Deployment", color: "#f3465a", icon: false},
      {title: "Website Revamp & Deployment", color: "#32adf4", icon: false},
      {title: "Website Revamp & Deployment", color: "#32adf4", icon: false},
    ];
  }

}
