import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileUploadElementComponent } from './file-upload-element.component';

describe('FileUploadElementComponent', () => {
  let component: FileUploadElementComponent;
  let fixture: ComponentFixture<FileUploadElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileUploadElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
