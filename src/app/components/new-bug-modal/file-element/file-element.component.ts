import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-file-element',
  templateUrl: './file-element.component.html',
  styleUrls: ['./file-element.component.scss']
})
export class FileElementComponent implements OnInit {
  @Input() data: string;
  show: boolean = true;
  
  constructor() { }

  ngOnInit() {
  }

  hide() {
    this.show = false;
  }
}
