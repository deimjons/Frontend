import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBugModalComponent } from './new-bug-modal.component';

describe('NewBugModalComponent', () => {
  let component: NewBugModalComponent;
  let fixture: ComponentFixture<NewBugModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewBugModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBugModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
