import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-bug-modal',
  templateUrl: './new-bug-modal.component.html',
  styleUrls: ['./new-bug-modal.component.scss']
})
export class NewBugModalComponent implements OnInit {
  tags: any[];
  files: any[];
  constructor() { 
    this.tags = [
      {name: "Bob Nilson", assigned: true},
      {name: "Igor Wang", assigned: false},
    ];

    this.files = [
      "fa fa-dropbox",
      "fa fa-dropbox",
      "fa fa-dropbox",
      "fa fa-dropbox",
      "fa fa-dropbox",
      "fa fa-dropbox",
    ];
  }

  ngOnInit() {

  }

}
