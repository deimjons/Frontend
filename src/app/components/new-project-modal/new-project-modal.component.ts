import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-project-modal',
  templateUrl: './new-project-modal.component.html',
  styleUrls: ['./new-project-modal.component.scss']
})
export class NewProjectModalComponent implements OnInit {
  tags: any[];
  constructor() { }

  ngOnInit() {
    this.tags = [
      {name: "Bob Nilson", assigned: true},
      {name: "Igor Wang", assigned: true},
    ];
  }

}
