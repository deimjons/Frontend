import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomInputElementComponent } from './custom-input-element.component';

describe('CustomInputElementComponent', () => {
  let component: CustomInputElementComponent;
  let fixture: ComponentFixture<CustomInputElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomInputElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomInputElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
