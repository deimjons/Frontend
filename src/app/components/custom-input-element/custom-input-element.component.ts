import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-custom-input-element',
  templateUrl: './custom-input-element.component.html',
  styleUrls: ['./custom-input-element.component.scss']
})
export class CustomInputElementComponent implements OnInit {
  @Input() field: string;
  @Input() type: string;
  @Input() id: string;
  required: any;
  item: any;

  constructor() { }

  ngOnInit() {
    this.required = {
      firstName: {min: 2, max: 35},
      lastName: {min: 2, max: 35},
      phone: {min: 8, max: 15},
      oldPass: {min: 8},
      newPass: {min: 8},
    };
    this.item = {pass: false, edit: true, error: false};
  }

  changeState(event) {
    switch(this.id) {
      case "first-name":
        if(event.path[0].value.length <= this.required.firstName.max && event.path[0].value.length >= this.required.firstName.min) {
          this.item = {pass: true, edit:false, error: false};
        } else if(event.path[0].value.length > this.required.firstName.max) {
          this.item = {pass: false, edit:false, error: true};
        } else {
          this.item = {pass: false, edit: true, error: false};
        }
        break;
      case "last-name":
        if(event.path[0].value.length <= this.required.lastName.max && event.path[0].value.length >= this.required.lastName.min) {
          this.item = {pass: true, edit:false, error: false};
        } else if(event.path[0].value.length > this.required.lastName.max) {
          this.item = {pass: false, edit:false, error: true};
        } else {
          this.item = {pass: false, edit: true, error: false};
        }
        break;
      case "phone":
        if(event.path[0].value.length <= this.required.phone.max && event.path[0].value.length >= this.required.phone.min) {
          this.item = {pass: true, edit:false, error: false};
        } else if(event.path[0].value.length > this.required.phone.max || (event.path[0].value.length < this.required.phone.min && event.path[0].value.length > 0)) {
          this.item = {pass: false, edit:false, error: true};
        } else {
          this.item = {pass: false, edit: true, error: false};
        }
        break;
      case "email":
        break;
      case "old-pass":
        if(event.path[0].value.length >= this.required.oldPass.min) {
          this.item = {pass: true, edit:false, error: false};
        } else if(event.path[0].value.length == 0) {
          this.item = {pass: false, edit: true, error: false};
        } else {
          this.item = {pass: false, edit: false, error: true};
        }
        break;
      case "new-pass":
        if(event.path[0].value.length >= this.required.newPass.min) {
          this.item = {pass: true, edit:false, error: false};
        } else if(event.path[0].value.length == 0) {
          this.item = {pass: false, edit: true, error: false};
        } else {
          this.item = {pass: false, edit: false, error: true};
        }
        break;
      case "confirm-pass":
        if(event.path[0].value.length >= this.required.newPass.min) {
          this.item = {pass: true, edit:false, error: false};
        } else if(event.path[0].value.length == 0) {
          this.item = {pass: false, edit: true, error: false};
        } else {
          this.item = {pass: false, edit: false, error: true};
        }
        break;
    }
  }

}
