import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class AppService {
  public page: any = new BehaviorSubject({page: 'dashboard', title: 'Dashboard'});
  public searchBar: any = new BehaviorSubject(false);
  public showSideBar: any = new BehaviorSubject(true);
  public showTable: any = new BehaviorSubject(0);
  projectTableData: any[];
  
  constructor() { 
    this.projectTableData = [
      {bugId: 746530, date: "16-05-16", title: "AirAsia Intranet System Trade", priority: 1, members: ["user1.png", "user2.png"], location: "Location", category: ["Home Page"], status: "New"},
      {bugId: 746530, date: "16-05-16", title: "AirAsia Intranet System Trade", priority: 2, members: ["user1.png", "user2.png"], location: "Location", category: ["Header"], status: "New"},
      {bugId: 746530, date: "16-05-16", title: "AirAsia Intranet System Trade", priority: 3, members: ["user1.png", "user2.png"], location: "Location", category: ["Landing Page"], status: "New"},
      {bugId: 746530, date: "16-05-16", title: "AirAsia Intranet System Trade", priority: 1, members: ["user1.png", "user2.png"], location: "Location", category: ["Home Page"], status: "New"},
      {bugId: 746530, date: "16-05-16", title: "AirAsia Intranet System Trade", priority: 1, members: ["user1.png", "user2.png"], location: "Location", category: ["Home Page"], status: "New"},
      {bugId: 746530, date: "16-05-16", title: "AirAsia Intranet System Trade", priority: 2, members: ["user1.png", "user2.png"], location: "Location", category: ["Home Page"], status: "New"},
      {bugId: 746530, date: "16-05-16", title: "AirAsia Intranet System Trade", priority: 1, members: ["user1.png", "user2.png"], location: "Location", category: ["Home Page"], status: "New"},
      {bugId: 746530, date: "16-05-16", title: "AirAsia Intranet System Trade", priority: 1, members: ["user1.png", "user2.png"], location: "Location", category: ["Home Page"], status: "New"},
      {bugId: 746530, date: "16-05-16", title: "AirAsia Intranet System Trade", priority: 1, members: ["user1.png", "user2.png"], location: "Location", category: ["Home Page"], status: "New"},
    ];
  }

  public setPage(page: string, title: string) {
    this.page.next({page: page, title: title});
  }

  public getPage() {
    return this.page;
  }

  public setSearchBar(flag: boolean) {
    this.searchBar.next(flag);
  }

  public showProjectTable(flag: boolean) {
    this.showTable.next(flag);
  }

  public getProjectTableData() {
    return this.projectTableData;
  }

  public setShowSideBar(flag) {
    this.showSideBar.next(flag);
  }

}
