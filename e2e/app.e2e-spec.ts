import { BugtrackerPage } from './app.po';

describe('bugtracker App', () => {
  let page: BugtrackerPage;

  beforeEach(() => {
    page = new BugtrackerPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
